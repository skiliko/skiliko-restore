#!/bin/bash

set -e

rm -rf backup
mkdir backup
aws s3 sync s3://$AWS_BUCKET/$BACKUP_NAME backup

for file in $(ls backup/*.tar); do
  database=${file/backup\//''}
  database=${database/.tar/''}

  restore/postgres.sh $database $file
done

for file in backup/*.tgz; do
  database=${file/backup\//''}
  database=${database/.tgz/''}

  restore/mongodb.sh $database $file
done