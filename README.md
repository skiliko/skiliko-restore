# Skiliko backup image

Download the database from s3 and reimport in the associated databases

Before any restore you need to close all connections to the databases but keep the databases container open
```
# no one access databases but databases started
docker-compose stop 
docker-compose start mongodb postgres
```

then when databases are up you can connect the containers with the restore image

```
docker run --rm \
  --link skiliko_postgres_1:postgres \
  --link skiliko_mongodb_1:mongodb \
  -e BACKUP_NAME=backup/2017-01-30T10:41:30Z \
  -e AWS_ACCESS_KEY_ID=XXX \
  -e AWS_SECRET_ACCESS_KEY=XXX \
  -e AWS_BUCKET=XXX \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_PASSWORD='' \
  registry.gitlab.com/skiliko/skiliko-restore
```