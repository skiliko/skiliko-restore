#!/bin/bash

database=$1
file=$2

psql \
  -h $POSTGRES_PORT_5432_TCP_ADDR \
  -p $POSTGRES_PORT_5432_TCP_PORT \
  -U $POSTGRES_USER \
  -c "drop database $database"

psql \
  -h $POSTGRES_PORT_5432_TCP_ADDR \
  -p $POSTGRES_PORT_5432_TCP_PORT \
  -U $POSTGRES_USER \
  -c "CREATE DATABASE $database WITH OWNER $POSTGRES_USER ENCODING 'UTF8';"

pg_restore \
  -h $POSTGRES_PORT_5432_TCP_ADDR \
  -p $POSTGRES_PORT_5432_TCP_PORT \
  -U $POSTGRES_USER \
  -d $database \
  -F tar \
  $file