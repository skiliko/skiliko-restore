#!/bin/bash

database=$1
file=$2

tar -zxf $file

mongo $database --eval "db.dropDatabase()" --host mongodb

mongorestore \
  -h mongodb \
  -d $database \
  dump/$database